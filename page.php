<?php
session_start();
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacte php</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body{ background: #354044; color: white;]        }
        label{color: white; font-size: 20px; font-family: Arial;}
        button{font-size: 40px; font-family: Arial; font-style: inherit;font-weight: bold;}
        h2{font-weight: bold; }
    </style>

</head>
<body>

<section id="contact" class="content-section">
    <div class="contact-section">
        <div class="container ">
            <div class="text-center">
                <h2>LET'S TALK</h2>
                <p class="text-">we help businesses organization and individuals from around the <br> world
                    bring their project to life. We'd love to help you too
                    </p>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-offset-3">


                    <?php if (array_key_exists('errors',$_SESSION)):?>

                            <div class="alert alert-danger">
                           <?= implode(  '<br>',$_SESSION['errors']);?>
                            </div>

                    <?php unset($_SESSION['errors']); endif;?>

                    <?php if (array_key_exists('succes',$_SESSION)):?>

                            <div class="alert alert-success">
                           votre message a bien ete envoyé
                            </div>

                    <?php endif;?>


                    <form class="form-horizontal " method="POST" action="message.php    " novalidate >

                        <div class="form-group ">
                            <label for="name" class="control-label" style="text-align: left">NAME*</label>
                            <input type="text" name="name" class="form-control" id="name"
                                   placeholder="Entrez votre Nom" required value="<?= isset($_SESSION['inputs']['name'])? $_SESSION['inputs']['name'] : ''?>">
                        </div>
                        <div class="form-group ">
                            <label for="email" class="control-label">EMAIL*</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="jane.doe@example.com" required value="<?= isset($_SESSION['inputs']['email'])? $_SESSION['inputs']['email'] : ''?>">
                        </div>
                        <div class="form-group ">
                            <label for="text" class="  control-label">HOW CAN WE HELPE ?*</label><br>
                            <textarea rows="10" cols="10" class="form-control" name="text" placeholder="Description" id="text" required> <?= isset($_SESSION['inputs']['text'])? $_SESSION['inputs']['text'] : ''?></textarea>
                        </div>
                        <button type="submit" class="btn btn-danger btn-block">SEND</button>
                    </form>

                </div>
            </div>
        </div>
</section>

</body>
</html>
<?php
unset($_SESSION['inputs']);
unset($_SESSION['errors']);
unset($_SESSION['succes']);